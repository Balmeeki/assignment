package com.websystique.springboot.model;

import java.util.Date;

public class Measurement {
    private Date timeStamp;
	private double temperature;
	private double dewPoint;
	private double precipitation;
	private double etc;
	private String timeStampS = timeStamp.toString();
	
	public Measurement (){
		timeStamp=null;
	}
	
	public Measurement (String timeStampS, double temperature, double dewPoint, double precipitation, double etc){
		this.timeStampS = timeStampS;
		this.temperature = temperature;
		this.dewPoint = dewPoint;
		this.precipitation = precipitation;
		this.etc = etc;
	}
	
	
	
	
/*	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Measurement other = (Measurement) obj;
		if (timeStamp != other.timeStamp)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Measurement[timeStamp=" + timeStamp + ", temperature=" + temperature + ", dewPoint=" + dewPoint
				+ ", precipitation=" + precipitation + ", etc=" + etc +"]";
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getDewPoint() {
		return dewPoint;
	}

	public void setDewPoint(double dewPoint) {
		this.dewPoint = dewPoint;
	}

	public double getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(double precipitation) {
		this.precipitation = precipitation;
	}

	public double getEtc() {
		return etc;
	}

	public void setEtc(double etc) {
		this.etc = etc;
	}

	public String getTimeStampS() {
		return timeStampS;
	}

	public void setTimeStampS(String timeStampS) {
		this.timeStampS = timeStampS;
	}

	

}
