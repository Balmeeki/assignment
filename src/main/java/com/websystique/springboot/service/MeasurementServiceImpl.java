package com.websystique.springboot.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.websystique.springboot.model.Measurement;




@Service("measurementService")
public class MeasurementServiceImpl implements MeasurementService{
	
	//private static final AtomicLong counter = new AtomicLong();
	
	private static List<Measurement> measurements;
	
	static{
		measurements= populateDummyMeasurements();
	}

	public Measurement findById(String timeStampS) {
		for(Measurement measurement : measurements){
			if(measurement.getTimeStampS() == timeStampS){
				return measurement;
			}
		}
		return null;
	}
	
	public void saveMeasurement (Measurement measurement) {
		Date timeStamp = new Date();
		String timeStampCurrent = timeStamp.toString();
		measurement.setTimeStampS(timeStampCurrent);
		measurements.add(measurement);
	}
	

	public boolean isUserExist(Measurement measurement) {
		return findById(measurement.getTimeStampS())!=null;
	}
	

	private static List<Measurement> populateDummyMeasurements(){
		List<Measurement> measurements = new ArrayList<Measurement>();
//		
		measurements.add(new Measurement("2015-09-01",22.4,18.6, 142.2,1234.56));
		return measurements;
	}

}
