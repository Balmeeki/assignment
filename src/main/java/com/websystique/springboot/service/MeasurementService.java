package com.websystique.springboot.service;





import com.websystique.springboot.model.Measurement;

public interface MeasurementService {
	
	Measurement findById(String timeStampS);
	
	
	void saveMeasurement(Measurement measurement);
	

	boolean isUserExist(Measurement measurement);
	
}
