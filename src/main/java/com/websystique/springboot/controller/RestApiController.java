package com.websystique.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.websystique.springboot.model.Measurement;

import com.websystique.springboot.service.MeasurementService;

import com.websystique.springboot.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	MeasurementService measurementService;
// -------------------Retrieve Single User------------------------------------------

	@RequestMapping(value = "/measurements/{timeStampS}", method = RequestMethod.GET)
	public ResponseEntity<?> getMeasurement(@PathVariable("timeStampS") String timeStampS) {
		logger.info("Fetching User with id {}", timeStampS);
		Measurement measurement = measurementService.findById(timeStampS);
		if (measurement == null) {
			logger.error("User with id {} not found.", timeStampS );
			return new ResponseEntity(new CustomErrorType("User with id " + timeStampS 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Measurement>(measurement, HttpStatus.OK);
	}

	// -------------------Create a User-------------------------------------------


	
	@RequestMapping(value = "/measurements/", method = RequestMethod.POST)
	public ResponseEntity<?> createMeasurements(@RequestBody Measurement measurement, UriComponentsBuilder ucBuilder) {
		logger.info("Creating User : {}", measurement);

		if (measurementService.isUserExist(measurement)) {
			logger.error("Unable to create. A Measurement with name {} already exist", measurement.getTimeStampS());
			return new ResponseEntity(new CustomErrorType("Unable to create. A User with name " + 
					measurement.getTimeStampS() + " already exist."),HttpStatus.CONFLICT);
		}
		measurementService.saveMeasurement(measurement);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/measurements/{timeStampS}").buildAndExpand(measurement.getTimeStampS()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}


}